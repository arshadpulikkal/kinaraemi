from setuptools import setup,find_packages

setup(name='Kinara',
      version='0.1',
      description='Kinara',
      author='Kinara',
      author_email='info@kinara.com',
      license='commercial',
      packages = find_packages(),
      zip_safe=False)

