import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'kinaracapital';
  //server = 'http://54.70.110.33:8000';
  //server = 'http://35.154.40.182:8000';
  //server = 'https://customerportal.kinaracapital.com/kinara'
  server = 'https://emiuat.kinaracapital.com/kinara'
  showCustomer = false;
  userInput: any = {};
  userDetails: any = {};
  captchaImg = '';
  captchatext = '';
  captchaImage = '';
  instance: any;
  apiInProgrss = false;
  razorpay = {
    key_id: 'rzp_test_egKvxj470W2Lec'
  };
  constructor(private http: HttpClient) {
    this.getCaptcha();
  }
  getCaptcha() {
    this.http.get(this.server + '/kinaracapital/api/home/getcaptcha').subscribe((resp: any) => {
      this.captchatext = resp.captchaText;
      this.captchaImg = resp.url;
      this.captchaImage = resp.captchaImage;
    });
  }
  refreshCaptcha() {
    this.getCaptcha();
  }
  getLoanDetails() {
    this.userInput.form1Error = '';
    if (!this.userInput.loanId) {
      this.userInput.form1Error = 'Please enter Loan Account Number';
      return;
    }
    if (!this.userInput.loanIdCC) {
      this.userInput.form1Error = 'Please re-enter Loan Account Number';
      return;
    }
    if (this.userInput.loanId !== this.userInput.loanIdCC) {
      this.userInput.form1Error = 'Loan Account Number and Re enter Loan Account Number are not matching';
      return;
    }
    // tslint:disable-next-line:triple-equals
    if (this.userInput.catpcha != this.captchatext) {
      this.userInput.form1Error = 'CAPTCHA validation failed';
      this.userInput.catpcha = '';
      this.getCaptcha();
      return;
    }
    this.apiInProgrss = true;
    this.http.get(this.server + '/kinaracapital/api/home/getcustomerdetails/?customerId=' +
      this.userInput.loanId + '&url=' + this.captchaImage).subscribe((resp: any) => {
        this.apiInProgrss = false;
        if (resp.status === 'success') {
          this.userDetails = resp.customerDetails;
          this.userDetails.amountType = 'full';
	  this.userDetails.email = "info@kinaracapital.com"
          this.userDetails.amountDue = parseFloat(this.userDetails.payOffAndDueAmount);
          this.showCustomer = true;
        } else if (resp.status === 'error') {
          this.userInput.form1Error = resp.ERROR;
        }
      }, (error) => {
        console.log(error);
        this.apiInProgrss = false;
      });
  }
  proceedPay() {
    this.userDetails.formError = '';
    let amountToPay = 0;
    if (this.userDetails.amountType === 'full') {
      amountToPay = this.userDetails.amountDue;
    } else {
      if (!this.userDetails.amountPay) {
        this.userDetails.formError = 'Please enter paying amount.';
        return;
      } else {
        amountToPay = this.userDetails.amountPay;
      }
    }
    this.userDetails.amount = amountToPay;
    this.http.post(this.server + '/kinaracapital/api/home/getorderid', this.userDetails).subscribe((resp: any) => {
      if (resp.status === 'success') {
        this.userDetails.orderId = resp.orderId;
        this.userDetails.loanAccountNumber = resp.loanAccountNumber;
        this.userDetails.description = "Due payment";
        setTimeout(() => {
          const element = document.getElementById('razorpay') as HTMLFormElement;
          element.submit();
        }, 100);
      } else if (resp.status === 'error') {
        this.userDetails.formError = resp.ERROR;
      }

      console.log(resp);
    });
  }
}
