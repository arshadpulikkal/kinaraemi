from django.contrib import admin
from django.conf.urls import include, url


from . import views

urlpatterns = [
#API Views
url(r'(?P<orderId>\w+)/success',        views.SuccessUrl,  name = 'SuccessUrl'),
#url(r'success',        views.SuccessUrl,  name = 'SuccessUrl'),
url(r'(?P<orderId>\w+)/failure',        views.FailureUrl,  name = 'FailureUrl'),
url(r'api/home/getcustomerdetails',        views.GetCustomerDetails,  name = 'GetCustomerDetails'),
url(r'api/home/getcaptcha',        views.GetCaptcha,  name = 'GetCaptcha'),
url(r'api/home/getorderid',        views.GetOrderId,  name = 'GetOrderId'),
url(r'api/home/getreport',        views.GetReport,  name = 'GetReport'),
]
