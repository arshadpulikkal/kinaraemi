import os
import ast
import json
import sys
from datetime import datetime, date
from bson.objectid import ObjectId
#django packages
from django.shortcuts import render,render_to_response,redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import views as auth_views
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User,Group
#from . import forms
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group
from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.conf import settings
from django.conf import settings as conf_setting
#rest_framework
from rest_framework import *
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.utils.dateparse import parse_datetime
from django.views.decorators.csrf import csrf_exempt

from captcha.image import ImageCaptcha
import random
import razorpay
import requests
from datetime import datetime ,timedelta
from Kinara.dataconnector import mongoConnector
mg = mongoConnector.MongoConnector.getInstance()                                                                                           
dbc = mg.getDatabaseClient('kinara')  
import base64
import os
from django.core.mail import EmailMessage
import pandas as pd
import ast
#serverIp = "https://produat.kinara.perdix.co.in:8443/perdix-server"
serverIp = "https://kinara.perdix.in/perdix-server"
#serverIp = "http://52.55.177.70:82/perdix-server_p2uat"
@csrf_exempt 
def SuccessUrl(request,orderId):
        qDict = {}
        qDict["id"] = orderId
        tmpDict = {}
	rDict = dict()
        tmpDict["updateDate"] = datetime.today()
	rDict["status"] = "success"
        tmpDict = dict()
	if request.method == 'POST':
		print request.POST.keys() 
		if request.POST.get("error[code]"):
			rDict["status"] = "error"
			rDict["ERROR"] = "RZ-Invalidresponse " + request.POST["error[code]"] + " Descrption" + request.POST["error[description]"]
               		tmpDict["status"] = "failure"
                	tmpDict["error"] = "RZ-Invalidresponse " + request.POST["error[code]"] + " Descrption " + request.POST["error[description]"]
		if request.POST.get('razorpay_payment_id') and request.POST.get('razorpay_order_id') and request.POST.get('razorpay_signature'):
			try:
				_params = dict()
				client = razorpay.Client(auth = ("rzp_live_7tOmn9jxIj18W6","V3zluTisQDQmH0muoKW80NG9"))
				_params["razorpay_payment_id"] = request.POST["razorpay_payment_id"]
				_params["razorpay_order_id"] = request.POST["razorpay_order_id"]
				_params["razorpay_signature"] = request.POST["razorpay_signature"]
				client.utility.verify_payment_signature(_params)
			except Exception as e:
				rDict["ERROR"] = "Error in razerpay signature verifications "+ str(e)			     
		                rDict["status"] = "error"
        collection = mg.getCollection('paymentInfo')
        docs = collection.find().sort([("date",-1)]).limit(1)
        docList = [i for i in docs]
        if docList:
            _params = docList[0]
            tmpDict["accountNumber"] = str(_params["accountId"])
            #tmpDict["accountNumber"] = "0001LTL1001721"
            tmpDict["transactionName"] = "Scheduled Demand"
            tmpDict["instrument"] = "NEFT"
            tmpDict["reference"] = str(orderId)
            tmpDict["repaymentDate"] = datetime.today().strftime("%Y-%m-%d")
            tmpDict["bankAccountNumber"] = "RazorpayAccount"
            tmpDict["instrumentDate"] = datetime.today().strftime("%Y-%m-%d")
            tmpDict["remarks"] = "paid by razorpay"
            if "productCode" in _params.keys():
                tmpDict["productCode"] = str(_params["productCode"])
            if "amount" in _params.keys():
                tmpDict["amount"] = _params["amount"]

            if "urnNo" in _params.keys():
                tmpDict["urnNo"] = _params["urnNo"]
	    if rDict["status"] == "error":
		tmpDict["status"] = "failure"
	    else:
                tmpDict["status"] = "success"
	try:
			params = dict()
			params["client_id"]="application"
			params["client_secret"] = "mySecretOAuthSecret"
			params["grant_type"] = "password"
			params["username"] = "website"
			params["password"] ="Kin@blr1"
			params["scope"] = "read write"
			loginResponse = requests.post(url=serverIp+"/oauth/token",data = params)
    	except Exception as e:
		    rDict["status"] = "error"
		    rDict["ERROR"] = "Internal server error login error "+str(e)
        	    return Response(rDict)
	loginDict = json.loads(loginResponse.text)
	access_token = loginDict["access_token"]
	if rDict["status"] != "error":
		try:
			response = requests.post(
                        serverIp +"/api/loanaccounts/repay",data = json.dumps(tmpDict), headers={'Authorization': 'Bearer '+access_token,'Content-Type': 'application/json'	})
                	_res= json.loads(response.text)
                	if "error" in _res.keys():
                    		raise Exception(_res["error"])
			rDict["status"] = "success"
		except Exception as e:
			rDict["status"] = "error"
			rDict["ERROR"] = "D-Invalidresponse " + str(e)
               		tmpDict["status"] = "failure"
                	tmpDict["error"] = "D-Invalidresponse " + str(e)
                	print rDict
        try:
            mg.getCollection("paymentInfo").update(
                    qDict,
                    {
                        "$set":tmpDict,
                    },
                    upsert= False
                    )
        except Exception as e:
            print "ERROR in update"
        if rDict["status"] == "error":
                return redirect("https://customerportal.kinaracapital.com/static/failed.html")
        else:
                return redirect("https://customerportal.kinaracapital.com/static/success.html")
def FailureUrl(request,orderId):
        qDict = {}
        qDict["orderId"] = orderId
        tmpDict = dict()
        tmpDict["status"] = "failure"
        tmpDict["error"] = "RP:Razorpay error"
        tmpDict["updateDate"] = datetime.today()
        try:
            mg.getCollection("paymentInfo").update(
                    qDict,
                    {
                        "$set":tmpDict,
                    },
                    upsert= True
                    )
        except Exception as e:
            print "ERROR in update"
	return redirect("https://customerportal.kinaracapital.com/static/failed.html")

@csrf_exempt
@api_view(['POST','GET'])
#@authentication_classes((SessionAuthentication, BasicAuthentication))
#@permission_classes((IsAuthenticated,))
def GetCustomerDetails(request):
        rDict = dict()
        rDict["status"] = "Error"
        registerParams = dict()
        if request.method == 'GET':
                customerId = str(request.GET["customerId"])
                print type(customerId),customerId
        if request.GET.get("url"):
                captcha = str(request.GET['url'])
                if os.path.exists(captcha):
                    os.remove(captcha)

        elif request.method == 'POST':
                registerParams = json.loads(request.body)
        else:
                reqDict = request.data
	if customerId:
		try:
			params = dict()
			params["client_id"]="application"
			params["client_secret"] = "mySecretOAuthSecret"
			params["grant_type"] = "password"
			params["username"] = "website"
			params["password"] ="Kin@blr1"
			params["scope"] = "read write"
			loginResponse = requests.post(url=serverIp + "/oauth/token",data = params)
		except Exception as e:
			rDict["ERROR"] = "Internal server error login error "+str(e)
			rDict["status"] = "error"
				
        		return Response(rDict)
		loginDict = json.loads(loginResponse.text)
		if "access_token" not in loginDict.keys():
			rDict["ERROR"] = "D-Internal server :login error "
			rDict["status"] = "error"
        		return Response(rDict)
		access_token = loginDict["access_token"]
		try:
			'''response = requests.get(
"http://52.55.177.70:82/perdix-server_p2uat/api/loanaccounts/show/accountId/?accountId=" + customerId, headers={'Authorization': 'Bearer '+access_token	})'''
                        response = requests.get(
serverIp +"/api/loanaccounts/show/accountId/?accountId=" + customerId, headers={'Authorization': 'Bearer '+access_token	})

		except Exception as e:
			rDict["status"] = "error"
			rDict["ERROR"] = "Invalid loan account number"
			return Response(rDict)
		tmpList = ['accountId','customerFirstName','customer1FirstName','customer1Address2','customer1CountryCode','customer1StateCode' ,"customer1CityCode" , "customer1PinCode" , "customer1Phone1","payOffAndDueAmount","productCode","customerId1"]
		responseData = json.loads(response.text)
                if "error" in responseData.keys():
                    	rDict["status"] = "error"
			rDict["ERROR"] = "Invalid loan account number"
			return Response(rDict)

		tmpDict = dict()
		for i in responseData.keys():
			if i in tmpList:		
				tmpDict[i] = responseData[i]
                tmpDict["payOffAndDueAmount"] = 0.0
                if "totalFeeDue" in responseData.keys():
                    tmpDict["payOffAndDueAmount"] += float(responseData["totalFeeDue"])
                if "totalDemandDue" in responseData.keys():
                    tmpDict["payOffAndDueAmount"] += float(responseData["totalDemandDue"])
                if "totalSecurityDepositDue" in responseData.keys():
                    tmpDict["payOffAndDueAmount"] += float(responseData["totalSecurityDepositDue"])
                if "bookedNotDuePenalInterest" in responseData.keys():
                    tmpDict["payOffAndDueAmount"] += float(responseData["bookedNotDuePenalInterest"])
                    tmpDict["payOffAndDueAmount"] = round(tmpDict["payOffAndDueAmount"])
                if "customerId1" in responseData.keys():
                        tmpDict["urnNo"] = responseData["customerId1"]

                if "customer1Phone1" in responseData.keys():
                        tmpDict["phoneNo"] = responseData["customer1Phone1"]
                        tmpDict["customer1Phone1"]  = "XXXXXX" + responseData["customer1Phone1"][-4] + responseData["customer1Phone1"][-3] + responseData["customer1Phone1"][-2] + responseData["customer1Phone1"][-1]
                if "customer1FirstName" in responseData.keys():
                        tmpDict["businessName"] = responseData["customer1FirstName"]
		if "customer2FirstName" in responseData.keys():
			tmpDict["customerName"] = responseData["customer2FirstName"]
		if "customer2MiddleName" in responseData.keys():
			if responseData["customer2MiddleName"]:
				tmpDict["customerName"] += responseData["customer2MiddleName"]
		if "customer2LastName" in responseData.keys():
			if responseData["customer2LastName"]:
				tmpDict["customerName"] += responseData["customer2LastName"]

		rDict["customerDetails"] = tmpDict
    		rDict["status"] = "success"
        return Response(rDict)
@api_view(['POST','GET'])
#@authentication_classes((SessionAuthentication, BasicAuthentication))
#@permission_classes((IsAuthenticated,))
def GetCaptcha(request):
        rDict = dict()
        rDict["status"] = "Error"
        registerParams = dict()
        number_list = ['0','1','2','3','4','5','6','7','8','9']
        captcha_string_list = []
        # Loop in the number list and return a digital captcha string list
        for i in range(5):
            char = random.choice(number_list)
            captcha_string_list.append(char)
        captcha_text = ''

        # Convert the digital list to string.    
        for item in captcha_string_list:
            captcha_text += str(item)
        image_captcha = ImageCaptcha()
        image = image_captcha.generate_image(captcha_text)
        # Add noise curve for the image.
        image_captcha.create_noise_curve(image, image.getcolors())
        image_captcha.create_noise_dots(image, image.getcolors())
	captcha = "captcha_"+captcha_text + ".png"
        image_file = "./kinara/static/"+captcha 
        image_captcha.write(captcha_text, image_file)
        image_captcha.write(captcha_text, image_file)
        rDict["status"] = "success"
        rDict["captchaText"] = captcha_text
        rDict["captchaImage"] = image_file
	rDict["url"] = "https://customerportal.kinaracapital.com/static/" + captcha
        return Response(rDict)
@api_view(['POST','GET'])
def GetOrderId(request):
  	rDict = dict()
	rDict["status"] = "error"
	if request.method == 'POST':
		record = request.data
		print record
		try:
			client = razorpay.Client(auth = ("rzp_live_7tOmn9jxIj18W6","V3zluTisQDQmH0muoKW80NG9"))
			print "created client"
			
			
		except Exception as e:
			rDict["ERROR"] = "Error in razerpay client "+ str(e)			     
		try:
                        amount = record["amount"]
                        tmpAmount = int(round(int(record["amount"]))*100)
                        print tmpAmount
			#order_amount = str(int(record["amount"]*100))
			order_amount = str(tmpAmount)
			order_currency = "INR"
			order_receipt = "order_rcptid_11"
			notes = dict()
                        print record.keys
			if "accountId"in record.keys():
				notes["loanAccountNumber"] = record["accountId"]
			tmpDict = dict()
			tmpDict["amount"]= order_amount
			tmpDict["currency"] = "INR"
			tmpDict["receipt"] = order_receipt
			tmpDict["notes"] = notes
			tmpDict["payment_capture"] = 0
                        if amount > record["payOffAndDueAmount"]:
                        	rDict["ERROR"] = "Please check amount due"		   
			        return Response(rDict)
                        if amount < 1:
                         	rDict["ERROR"] = "Please check amount due"		   
			        return Response(rDict)
                        _t = dict()
			_t = client.order.create(tmpDict)
			rDict["orderId"] = _t["id"]
			rDict["loanAccountNumber"] = notes["loanAccountNumber"]
                        record.update(_t)
                        collection = mg.getCollection("paymentInfo")
                        record["updateDate"] = datetime.today()
                        record["date"] = datetime.today()
                        record["status"] = "Attempted"
                        record["amount"] = amount
                        if collection is None:
                            print "ERROR:Collection None"
                        mg.updateCollection_dash({"orderId":record["id"]},"paymentInfo",record)
		except Exception as e:
			rDict["ERROR"] = "Error in razerpay create order id "+ str(e)		   
			print rDict
			return Response(rDict)
	rDict["status"]	 = "success"
	return Response(rDict)
@api_view(['POST','GET'])
def GetReport(request):
    rDict = dict()
    #if request.method.GET:

    try:
        qDict = dict()
        now = datetime.today()
        #qDict["updateDate"] = {"$lte": now,"$gte": now -timedelta(1)}
        collection = mg.getCollection('paymentInfo')
        doc = collection.find(qDict)
        docList = [i for i in doc ]
        print docList
        if docList:
            df = pd.DataFrame(docList)
            df = df.sort_values(by = ['updateDate'],ascending=False)
            df_sort = df.sort_values(by = ['updateDate'],ascending=False)
            df_sort = df_sort[["updateDate","accountId","productCode","amount","amountDue","amountType","customer1Phone1","customerName","id","status","error"]]
            df_sort.columns = ["Date","AccountId","ProductCode","Amount","AmountDue","AmountType","Phone","CustomerName","OrderId","status","error"]
            attachment_path = "report/tansaction_" + datetime.today().strftime("%Y-%m-%d")+'.xlsx'
            df_sort.to_excel(attachment_path , engine='xlsxwriter',index=False)
    except Exception as e:
        rDict["status"]  = "Error"   
        rDict["ERROR:"]  = "Error in db fetching  " + str(e)
        print rDict
    try:
        mimetype='application/octet-stream'
        email = EmailMessage("Daily transaction report ", "PFA", to=["arshad@forthcode.com"])
        file_name = os.path.basename(attachment_path)
        data = open(attachment_path, 'rb').read()
        encoded = base64.b64encode(data)
        email.attach(file_name, encoded, mimetype=mimetype)
        email.send(fail_silently= False)
    except Exception as e:
        print "Error in mail communication " + str(e)
    file_path = attachment_path
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
        return response
